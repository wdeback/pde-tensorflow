#!/usr/bin/env python-glibc2.17
# coding: utf-8

#get_ipython().magic('matplotlib inline')
import matplotlib
matplotlib.use('pdf')
import matplotlib.pylab as plt

import tensorflow as tf
import numpy as np

import time

import argparse
parser = argparse.ArgumentParser(description='Simulate Gierer/Meinhardt reaction-diffusion system on GPU using tensorflow.')
parser.add_argument('-x', '--x', type=int, help='x size of domain', required=True)
parser.add_argument('-y', '--y', type=int, help='y size of domain', required=True)
parser.add_argument('-z', '--z', type=int, help='z size of domain', required=False, default=0)
parser.add_argument('-k', '--kappa', type=float, help='Saturation constant', required=False, default=0.10)
parser.add_argument('--Da', type=float, help='Diffusion of activator (Di = 100*Da)', required=False, default=0.50)
parser.add_argument('-o', '--output', type=str, help='Output filename (.tif)', default='MG')
args = parser.parse_args()

import os
outputdir = './output_k={}_D={}'.format(args.kappa, args.Da)
if not os.path.exists(outputdir):
    os.mkdir(outputdir)

#Imports for visualization
#import PIL.Image
#from io import BytesIO
#from IPython.display import clear_output, Image, display



# In[11]:

#def DisplayArray(a, fmt='png', rng=[0,1]):
#  """Display an array as a picture."""
#  a = (a - rng[0])/float(rng[1] - rng[0])*255
#  a = np.uint8(np.clip(a, 0, 255))
#  f = BytesIO()
#  PIL.Image.fromarray(a).save(f, fmt)
#  clear_output(wait = True)
#  display(Image(data=f.getvalue()))


# In[12]:

sess = tf.InteractiveSession()


# In[ ]:

def make_kernel(a):
  """Transform a 2D array into a convolution kernel"""
  a = np.asarray(a)
  a = a.reshape(list(a.shape) + [1,1])
  return tf.constant(a, dtype=1)

def simple_conv(x, k):
  """A simplified 2D convolution operation"""
  x = tf.expand_dims(tf.expand_dims(x, 0), -1)
  y = tf.nn.depthwise_conv2d(x, k, [1, 1, 1, 1], padding='SAME')
  return y[0, :, :, 0]

def simple_conv_periodic(x, k):
    """A simplified 2D convolution operation, with periodic boundaries"""
    x_shape = tf.shape(x)
    a = tf.tile(x, [3, 3]) # copy image into a 3x3
    x = a[x_shape[0]-2:x_shape[0]*2+2, 
          x_shape[1]-2:x_shape[1]*2+2] # cut out relevant part in the middle
    
    x = tf.expand_dims(tf.expand_dims(x, 0), -1) # add dimensions for batch_size and channels 
    y = tf.nn.conv2d(x, k, [1, 1, 1, 1], padding='VALID')
    print(tf.shape(y))
    return y[0, 1:-1, 1:-1, 0]

def laplace(x):
  """Compute the 2D laplacian of an array"""
  laplace_k = make_kernel([[0.5, 1.0, 0.5],
                           [1.0, -6., 1.0],
                           [0.5, 1.0, 0.5]])
  return simple_conv_periodic(x, laplace_k)


# In[20]:


def make_kernel_3d(a):
  """Transform a 3D array into a convolution kernel"""
  a = np.asarray(a)
  a = a.reshape(list(a.shape) + [1,1])
  return tf.constant(a, dtype=1)

def simple_conv_periodic_3d(x, k):
    """A simplified 3D convolution operation, with periodic boundaries"""
    x_shape = tf.shape(x)
    a = tf.tile(x, [3, 3, 3]) # copy image into a 3x3x3
    x = a[x_shape[0]-2:x_shape[0]*2+2, 
          x_shape[1]-2:x_shape[1]*2+2, 
          x_shape[2]-2:x_shape[2]*2+2] # cut out relevant part in the middle
    
    x = tf.expand_dims(tf.expand_dims(x, 0), -1) # add dimensions for batch_size and channels 
    y = tf.nn.conv3d(x, k, [1, 1, 1, 1, 1], padding='VALID')
    print(tf.shape(y))
    return y[0, 1:-1, 1:-1, 1:-1, 0]


def laplace_3d(x):
  """Compute the 3D laplacian of an array"""
#  laplace_k = make_kernel_3d([[[0.0, 0.0, 0.0],
#                           [0.0, 1.0, 0.0],
#                           [0.0, 0.0, 0.0]]
#                          ,
#                          [[0.0, 1.0, 0.0],
#                           [1.0, -6., 1.0],
#                           [0.0, 1.0, 0.0]]
#                          ,
#                          [[0.0, 0.0, 0.0],
#                           [0.0, 1.0, 0.0],
#                           [0.0, 0.0, 0.0]]])
  laplace_k = make_kernel_3d([[[0.25, 0.50, 0.25],
                               [0.50, 1.00, 0.50],
                               [0.25, 0.50, 0.25]]
                              ,
                              [[0.50, 1.00, 0.50],
                               [1.00,-14.0, 1.00],
                               [0.50, 1.00, 0.50]]
                              ,
                              [[0.25, 0.50, 0.25],
                               [0.50, 1.00, 0.50],
                               [0.25, 0.50, 0.25]]])
  return simple_conv_periodic_3d(x, laplace_k)


# In[21]:

X,Y,Z = args.x, args.y, args.z

three_dim = False
if args.z > 0:
    three_dim = True

# Set everything to zero
if three_dim:
    #a_init = np.zeros([X, Y, Z], dtype=np.float32)
    a_init = np.random.normal(loc=0.5, scale=0.1, size=[X, Y, Z])
    i_init = np.ones( [X, Y, Z], dtype=np.float32)
else:
    #a_init = np.zeros([X, Y], dtype=np.float32)
    a_init = np.random.normal(loc=0.5, scale=0.1, size=[X, Y])
    i_init = np.ones( [X, Y], dtype=np.float32)
    
a_init = np.array(a_init, dtype=np.float32)

i_init *= 0.1

# Create variables for simulation state
A = tf.Variable(a_init)
I = tf.Variable(i_init)

# Parameters
dt    = tf.placeholder(tf.float32, shape=())
rho   = tf.placeholder(tf.float32, shape=())
kappa = tf.placeholder(tf.float32, shape=())
rho_a = tf.placeholder(tf.float32, shape=())
mu_i  = tf.placeholder(tf.float32, shape=())
mu_a  = tf.placeholder(tf.float32, shape=())
alpha_a = tf.placeholder(tf.float32, shape=())
alpha_i = tf.placeholder(tf.float32, shape=())

stoptime = 3000
ddt = 0.05
dx = 2.0
Da = args.Da
Di = 100.0*Da
alphaa = np.float32((Da * ddt) / (dx*dx))
alphai = np.float32((Di * ddt) / (dx*dx))
print(alphaa, alphai)

# Discretized PDE equations
if three_dim:
    A_ = A + dt * (alpha_a*laplace_3d(A) + (rho/I)*((A*A)/(1 + kappa*A*A)) - mu_a * A + rho_a)
    I_ = I + dt * (alpha_i*laplace_3d(I) + rho*((A*A)/(1+kappa*A*A)) - mu_i * I)
else:
    A_ = A + dt * (alpha_a*laplace(A) + (rho/I)*((A*A)/(1 + kappa*A*A)) - mu_a * A + rho_a)
    I_ = I + dt * (alpha_i*laplace(I) + rho*((A*A)/(1+kappa*A*A)) - mu_i * I)


# Operation to update the state
step = tf.group(
    A.assign(A_),
    I.assign(I_),
    tf.check_numerics(A, 'A contains inf or nan'),
    tf.check_numerics(I, 'I contains inf or nan')
    )



#from ipywidgets import FloatProgress
#from IPython.display import display
#f = FloatProgress(min=0, max=stoptime)
#display(f)

#fig, ax = plt.subplots(ncols=2)
#fig.tight_layout()
#cax0 = ax[0].imshow(a_init[int(X/2)])
#cax1 = ax[1].imshow(i_init[int(X/2)])
#fig.colorbar(cax0)

# Initialize state to initial conditions
tf.global_variables_initializer().run()

# Run 1000 steps of PDE
count_fig = 0
start = time.time()
for i in range(int(float(stoptime)/ddt)):
    #f.value = i
    # Step simulation
    step.run({
            dt:      ddt,
            rho:     0.001,
            rho_a:   0.001,
            kappa:   args.kappa,
            mu_i:    0.03,
            mu_a:    0.02,
            alpha_a: alphaa,
            alpha_i: alphai})
    if i % 1000 == 0:
        print('.', end='')
        arr = A.eval()
        #zslice = arr[int(X/2)] 
        #DisplayArray(zslice, rng=[0.0, 1.0])
        #cax0.set_data(zslice)
        #cax1.set_data(zslice)
        #fig.savefig('{}.png'.format(count_fig), dpi=300)
        count_fig+=1
        # dump array to pickle
        #arr.dump('AI_{:03d}.pkl'.format(count_fig))

        # write to TIF
        from skimage.external import tifffile 
        outputfilename = os.path.join(outputdir, '{}_{:03d}.tif'.format(args.output, count_fig))
        tifffile.imsave(outputfilename, arr, compress=6)

end = time.time()
print('exec time = {:.02f} sec'.format(end-start))     



