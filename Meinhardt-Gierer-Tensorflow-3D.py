import tensorflow as tf
import numpy as np
from skimage.external import tifffile
import time

#%%
import argparse
parser = argparse.ArgumentParser(description='Simulate 3D Gierer/Meinhardt reaction-diffusion system on GPU using tensorflow.')
parser.add_argument('-n', '--numsteps', type=int, help='Number of steps', required=True)
parser.add_argument('-x', '--x', type=int, help='x size of domain', required=True)
parser.add_argument('-y', '--y', type=int, help='y size of domain', required=True)
parser.add_argument('-z', '--z', type=int, help='z size of domain', required=True)
parser.add_argument('-D', '--D', type=float, help='diffusion param', required=True, default=0.05)
parser.add_argument('-k', '--kappa', type=float, help='saturation param', required=False, default=0.13)
args = parser.parse_args()

#%%

def make_kernel(a):
  """Transform ndarray into convolution kernel"""
  a = np.asarray(a)
  a = a.reshape(list(a.shape) + [1,1])
  return tf.constant(a, dtype=tf.float32)

def conv3D_periodic(x, k):
    """A simplified 3D convolution operation, with periodic boundaries"""
    x_shape = tf.shape(x)
    a = tf.tile(x, [3, 3, 3]) # copy image into a 3x3x3
    x = a[x_shape[0]-2:x_shape[0]*2+2, 
          x_shape[1]-2:x_shape[1]*2+2, 
          x_shape[2]-2:x_shape[2]*2+2] # cut out relevant part in the middle
    x = tf.expand_dims(tf.expand_dims(x, 0), -1) # add fake dimensions
    y = tf.nn.conv3d(x, k, [1, 1, 1, 1, 1], padding='VALID')
    return y[0, 1:-1, 1:-1, 1:-1, 0]

def laplace_3d(x):
  laplace_k = make_kernel([[[0.25, 0.5,  0.25],
                            [0.5,  1.,   0.5 ],
                            [0.25, 0.5,  0.25]],
                           [[0.5,  1.,   0.5 ],
                            [1.,  -14. , 1.  ],
                            [0.5,  1.,   0.5 ]],
                           [[0.25, 0.50, 0.25],
                            [0.5,  1.,   0.5 ],
                            [0.25, 0.5,  0.25]]])
  return conv3D_periodic(x, laplace_k)

#%%

# Lattice size
X,Y,Z = args.x, args.y, args.z
print('Domain: ',X, Y, Z)
# Discretization params
ddt      =  0.05# time step
dx       =  2.0 # spatial discretization
Da       =  0.05 # diffusion A
Di       =  100.0*Da # diffusion I
alphaa = np.float32((Da * ddt) / (dx*dx))
alphai = np.float32((Di * ddt) / (dx*dx))

print('Da = ',Da)

# Initial conditions: A: uniform random, I: 0.1 
a_init = np.random.normal(loc=0.5, scale=0.1, size=[X, Y, Z])
a_init = np.array(a_init, dtype=np.float32)
i_init = np.ones([X, Y, Z], dtype=np.float32)
i_init *= 0.1

#%%

# Create variables for simulation state
A = tf.Variable(a_init)
I = tf.Variable(i_init)

# Parameters
dt    = tf.placeholder(tf.float32, shape=())
rho   = tf.placeholder(tf.float32, shape=())
kappa = tf.placeholder(tf.float32, shape=())
rho_a = tf.placeholder(tf.float32, shape=())
mu_i  = tf.placeholder(tf.float32, shape=())
mu_a  = tf.placeholder(tf.float32, shape=())
alpha_a = tf.placeholder(tf.float32, shape=())
alpha_i = tf.placeholder(tf.float32, shape=())

#%%
with tf.name_scope('PDE_model'):
    # Discretized PDE equations
    A_ = A + dt * (alpha_a*laplace_3d(A) + (rho/I)*((A*A)/(1 + kappa*A*A)) - mu_a * A + rho_a)
    I_ = I + dt * (alpha_i*laplace_3d(I) + rho*((A*A)/(1+kappa*A*A)) - mu_i * I)    


with tf.name_scope('Step'):
    # Operation to update the state
    step = tf.group(
      A.assign(A_),
      I.assign(I_)
    )
    
with tf.name_scope('Check'):
    # Check for NaNs and Infs
    check = tf.group(
      tf.check_numerics(A, message='Error: NaNs of Infs in A'),
      tf.check_numerics(I, message='Error: NaNs of Infs in I')
    )

#%%

start = time.time()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for i in range(args.numsteps):
        sess.run(step, feed_dict={
                                dt:      ddt,
                                rho:     0.001,
                                rho_a:   0.001,
                                kappa:   args.kappa,
                                mu_i:    0.03,
                                mu_a:    0.02,
                                alpha_a: alphaa,
                                alpha_i: alphai})
        
        if i % 1000 == 0:
            # export to TIFF file
            tifffile.imsave('AI_{:06d}_k={:.02f}_D={:.02f}.tif'.format(i, args.kappa, args.D), sess.run(A), compress=6)
            
            end = time.time()
            print('Exec time (1000 steps)= {:.04f} sec'.format(end-start))
            start = time.time()

#%%



