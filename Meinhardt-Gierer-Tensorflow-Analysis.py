#!/usr/bin/env python-glibc2.17
# coding: utf-8

#get_ipython().magic('matplotlib inline')
import matplotlib
matplotlib.use('pdf')
import matplotlib.pylab as plt

import tensorflow as tf
import numpy as np

import time

import argparse
parser = argparse.ArgumentParser(description='Simulate Gierer/Meinhardt reaction-diffusion system on GPU using tensorflow.')
parser.add_argument('-x', '--x', type=int, help='x size of domain', required=True)
parser.add_argument('-y', '--y', type=int, help='y size of domain', required=True)
parser.add_argument('-z', '--z', type=int, help='z size of domain', required=False, default=0)
parser.add_argument('-k', '--kappa', type=float, help='saturation parameter (default = 0.15)', required=False, default=0.15)
parser.add_argument('-o', '--output', type=str, help='Output filename (.tif)', default='MG')
args = parser.parse_args()

import os
outputdir = './output_sweep'
if not os.path.exists(outputdir):
    os.mkdir(outputdir)

#Imports for visualization
#import PIL.Image
#from io import BytesIO
#from IPython.display import clear_output, Image, display



# In[11]:

#def DisplayArray(a, fmt='png', rng=[0,1]):
#  """Display an array as a picture."""
#  a = (a - rng[0])/float(rng[1] - rng[0])*255
#  a = np.uint8(np.clip(a, 0, 255))
#  f = BytesIO()
#  PIL.Image.fromarray(a).save(f, fmt)
#  clear_output(wait = True)
#  display(Image(data=f.getvalue()))


# In[12]:

sess = tf.InteractiveSession()


# In[ ]:

def make_kernel(a):
  """Transform a 2D array into a convolution kernel"""
  a = np.asarray(a)
  a = a.reshape(list(a.shape) + [1,1])
  return tf.constant(a, dtype=1)

def simple_conv(x, k):
  """A simplified 2D convolution operation"""
  x = tf.expand_dims(tf.expand_dims(x, 0), -1)
  y = tf.nn.depthwise_conv2d(x, k, [1, 1, 1, 1], padding='SAME')
  return y[0, :, :, 0]

def simple_conv_periodic(x, k):
    """A simplified 2D convolution operation, with periodic boundaries"""
    x_shape = tf.shape(x)
    a = tf.tile(x, [3, 3]) # copy image into a 3x3
    x = a[x_shape[0]-2:x_shape[0]*2+2, 
          x_shape[1]-2:x_shape[1]*2+2] # cut out relevant part in the middle
    
    x = tf.expand_dims(tf.expand_dims(x, 0), -1) # add dimensions for batch_size and channels 
    y = tf.nn.conv2d(x, k, [1, 1, 1, 1], padding='VALID')
    print(tf.shape(y))
    return y[0, 1:-1, 1:-1, 0]

def laplace(x):
  """Compute the 2D laplacian of an array"""
  laplace_k = make_kernel([[0.5, 1.0, 0.5],
                           [1.0, -6., 1.0],
                           [0.5, 1.0, 0.5]])
  return simple_conv_periodic(x, laplace_k)


# In[20]:


def make_kernel_3d(a):
  """Transform a 3D array into a convolution kernel"""
  a = np.asarray(a)
  a = a.reshape(list(a.shape) + [1,1])
  return tf.constant(a, dtype=1)

def simple_conv_periodic_3d(x, k):
    """A simplified 3D convolution operation, with periodic boundaries"""
    x_shape = tf.shape(x)
    a = tf.tile(x, [3, 3, 3]) # copy image into a 3x3x3
    x = a[x_shape[0]-2:x_shape[0]*2+2, 
          x_shape[1]-2:x_shape[1]*2+2, 
          x_shape[2]-2:x_shape[2]*2+2] # cut out relevant part in the middle
    
    x = tf.expand_dims(tf.expand_dims(x, 0), -1) # add dimensions for batch_size and channels 
    y = tf.nn.conv3d(x, k, [1, 1, 1, 1, 1], padding='VALID')
    print(tf.shape(y))
    return y[0, 1:-1, 1:-1, 1:-1, 0]


def laplace_3d(x):
  """Compute the 3D laplacian of an array"""
#  laplace_k = make_kernel_3d([[[0.0, 0.0, 0.0],
#                           [0.0, 1.0, 0.0],
#                           [0.0, 0.0, 0.0]]
#                          ,
#                          [[0.0, 1.0, 0.0],
#                           [1.0, -6., 1.0],
#                           [0.0, 1.0, 0.0]]
#                          ,
#                          [[0.0, 0.0, 0.0],
#                           [0.0, 1.0, 0.0],
#                           [0.0, 0.0, 0.0]]])
  laplace_k = make_kernel_3d([[[0.25, 0.50, 0.25],
                               [0.50, 1.00, 0.50],
                               [0.25, 0.50, 0.25]]
                              ,
                              [[0.50, 1.00, 0.50],
                               [1.00,-14.0, 1.00],
                               [0.50, 1.00, 0.50]]
                              ,
                              [[0.25, 0.50, 0.25],
                               [0.50, 1.00, 0.50],
                               [0.25, 0.50, 0.25]]])
  return simple_conv_periodic_3d(x, laplace_k)


# In[21]:

X,Y,Z = args.x, args.y, args.z

three_dim = False
if args.z > 0:
    three_dim = True

# Set everything to zero
if three_dim:
    #a_init = np.zeros([X, Y, Z], dtype=np.float32)
    a_init = np.random.normal(loc=0.5, scale=0.1, size=[X, Y, Z])
    i_init = np.ones( [X, Y, Z], dtype=np.float32)
else:
    #a_init = np.zeros([X, Y], dtype=np.float32)
    a_init = np.random.normal(loc=0.5, scale=0.1, size=[X, Y])
    i_init = np.ones( [X, Y], dtype=np.float32)
    
a_init = np.array(a_init, dtype=np.float32)

i_init *= 0.1

# Create variables for simulation state
A = tf.Variable(a_init)
I = tf.Variable(i_init)

# Parameters
dt    = tf.placeholder(tf.float32, shape=())
rho   = tf.placeholder(tf.float32, shape=())
kappa = tf.placeholder(tf.float32, shape=())
rho_a = tf.placeholder(tf.float32, shape=())
mu_i  = tf.placeholder(tf.float32, shape=())
mu_a  = tf.placeholder(tf.float32, shape=())
alpha_a = tf.placeholder(tf.float32, shape=())
alpha_i = tf.placeholder(tf.float32, shape=())

stoptime = 3000
ddt = 0.05
dx = 4.0
Da = 0.5
Di = 50.0
alphaa = np.float32((Da * ddt) / (dx*dx))
alphai = np.float32((Di * ddt) / (dx*dx))
print(alphaa, alphai)

# Discretized PDE equations
if three_dim:
    A_ = A + dt * (alpha_a*laplace_3d(A) + (rho/I)*((A*A)/(1 + kappa*A*A)) - mu_a * A + rho_a)
    I_ = I + dt * (alpha_i*laplace_3d(I) + rho*((A*A)/(1+kappa*A*A)) - mu_i * I)
else:
    A_ = A + dt * (alpha_a*laplace(A) + (rho/I)*((A*A)/(1 + kappa*A*A)) - mu_a * A + rho_a)
    I_ = I + dt * (alpha_i*laplace(I) + rho*((A*A)/(1+kappa*A*A)) - mu_i * I)


# Operation to update the state
step = tf.group(
    A.assign(A_),
    I.assign(I_),
    tf.check_numerics(A, 'A contains inf or nan'),
    tf.check_numerics(I, 'I contains inf or nan')
    )



#from ipywidgets import FloatProgress
#from IPython.display import display
#f = FloatProgress(min=0, max=stoptime)
#display(f)

#fig, ax = plt.subplots(ncols=2)
#fig.tight_layout()
#cax0 = ax[0].imshow(a_init[int(X/2)])
#cax1 = ax[1].imshow(i_init[int(X/2)])
#fig.colorbar(cax0)

# Initialize state to initial conditions
tf.global_variables_initializer().run()

# Run 1000 steps of PDE
count_fig = 0
start = time.time()
for i in range(int(float(stoptime)/ddt)):
    #f.value = i
    # Step simulation
    step.run({
            dt:      ddt,
            rho:     0.001,
            rho_a:   0.001,
            kappa:   args.kappa,
            mu_i:    0.03,
            mu_a:    0.02,
            alpha_a: alphaa,
            alpha_i: alphai})
    if i % 2000 == 0:
        print('.', end='')
        arr = A.eval()
        #zslice = arr[int(X/2)] 
        #DisplayArray(zslice, rng=[0.0, 1.0])
        #cax0.set_data(zslice)
        #cax1.set_data(zslice)
        #fig.savefig('{}.png'.format(count_fig), dpi=300)
        count_fig+=1
        # dump array to pickle
        #arr.dump('AI_{:03d}.pkl'.format(count_fig))

        # write to TIF
        from skimage.external import tifffile 
        outputfilename = os.path.join(outputdir, '{}_{:.02f}_{:03d}.tif'.format(args.output, args.kappa, count_fig))
        tifffile.imsave(outputfilename, arr, compress=6)

end = time.time()
print('exec time = {:.02f} sec'.format(end-start))     


def load_image(filename):
    from skimage.external import tifffile
    return tifffile.imread(filename)

def resize(image, zoom_factor, order):
    from scipy.ndimage.interpolation import zoom
    image = zoom(image, zoom=zoom_factor, order=order)
    return image

def save_image(filename):
    from skimage.external import tifffile
    tifffile.imsave(filename, image, compress=6)

def threshold(image, threshold):
    im = np.array(image, copy=True)  
    im[ im >  threshold ] = 255
    im[ im <= threshold ] = 0
    return im

def fractal_dimension(image, plot=False):
    # computing the fractal dimension
    #considering only scales in a logarithmic list
    Lx=image.shape[0]
    Ly=image.shape[1]
    Lz=image.shape[2]
    maxsize = np.amin(image.shape)
    #print(maxsize)
    scales  = np.logspace(1, np.log2(maxsize), num=20, endpoint=False, base=2)
    Ns=[]
    #print(scales)
    # looping over several scales
    for scale in scales:
        bins=[ np.arange(0,Lx,scale),np.arange(0,Ly,scale),np.arange(0,Lz,scale) ]
        # computing the histogram
        im_coords = np.transpose(np.nonzero(image))
        H, edges=np.histogramdd(im_coords, bins=bins)
        #print(scale, Ns)
        Ns.append(np.sum(H>0))

    # linear fit, polynomial of degree 1
    coeffs=np.polyfit(np.log(scales), np.log(Ns), 1)

    if plot:
        plt.plot(np.log(scales),np.log(Ns), 'o', mfc='none')
        plt.plot(np.log(scales), np.polyval(coeffs,np.log(scales)))
        plt.xlabel('log $\epsilon$')
        plt.ylabel('log N')

    print(("The Hausdorff dimension is {}".format(-coeffs[0]))) #the fractal dimension is the OPPOSITE of the fitting coefficient
    return -coeffs[0]

def volume_ratio(image):
    return np.count_nonzero(image) / np.product(image.shape)

def surface_volume_ratio(image):
    from scipy.ndimage.morphology import binary_erosion
    image = np.array(image, dtype=np.bool)
    eroded = binary_erosion(image)
    surface = image-eroded
    #print("Surface = ", np.count_nonzero(surface))
    if np.count_nonzero(image) > 0:
        return np.count_nonzero(surface) / np.count_nonzero(image)
    else:
        return np.nan
    
def count_parts(image):
    from skimage.measure import label
    lab_im, num = label(image, return_num=True)
    #print(np.unique(lab_im))
    return num-1

## ========= ANALYSE WITH DIFFERENT THRESHOLDS ==========

im = A.eval()

results = []
for t in np.linspace(np.amin(im),np.amax(im),10):
    imt = threshold(im, t)

    V_r  = volume_ratio(imt)
    SV_r = surface_volume_ratio(imt)
    N    = count_parts(imt)
    D_f  = fractal_dimension(imt)    

    results.append([t, N, V_r, SV_r, D_f])
    
    #plt.figure()
    #plt.imshow(imt[12])
    #plt.title('N = {:d}, Vol. ratio = {:.02f}, Surf/Vol = {:.02f}, D_f = {:.02f}'.format(N, V_r, SV_r, D_f))

results = np.array(results)
import pandas as pd
df = pd.DataFrame(results, columns=('Threshold', 'Number of components', 'Volume ratio', 'Surface-to-Volume ratio', 'Fractal dimension'))

outputfilename = os.path.join(outputdir, '{}_{:.02f}_{:03d}.csv'.format(args.output, args.kappa, count_fig))
df.to_csv(outputfilename)



zoom_factor = 4.0
order = 3 # bicubic interpolation
im = A.eval() 
im_large = resize(im, zoom_factor=zoom_factor, order=order)
#im_t = threshold(im_large, 3.0)
outputfilename = os.path.join(outputdir, '{}_{:.02f}_{:03d}_large.tif'.format(args.output, args.kappa, count_fig))
tifffile.imsave(outputfilename, im_large, compress=6)




results = []
for t in np.linspace(np.amin(im),np.amax(im),10):

    V_r  = volume_ratio(im_t)
    SV_r = surface_volume_ratio(imt_)
    N    = count_parts(im_t)
    D_f  = fractal_dimension(im_t)    

    results.append([t, N, V_r, SV_r, D_f])
    
    #plt.figure()
    #plt.imshow(imt[12])
    #plt.title('N = {:d}, Vol. ratio = {:.02f}, Surf/Vol = {:.02f}, D_f = {:.02f}'.format(N, V_r, SV_r, D_f))

results = np.array(results)
import pandas as pd
df = pd.DataFrame(results, columns=('Threshold', 'Number of components', 'Volume ratio', 'Surface-to-Volume ratio', 'Fractal dimension'))

outputfilename = os.path.join(outputdir, '{}_{:.02f}{:03d}_large.csv'.format(args.output, args.kappa, count_fig))
df.to_csv(outputfilename)

